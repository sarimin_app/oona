//
//  CustomUserDefaults.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 9/11/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import Foundation

class CustomUserDefaults{
    
    static let shared = CustomUserDefaults()
    
    let KEY_TAB_OPEN = "tab_open"
    let KEY_TAB_VERSION = "tab_version"
    let KEY_ADS_NUMBER_ONTOP = "ads_ontop"
    let KEY_ADS_NUMBER = "ads_number"
    let KEY_MOVIE_NUMBER = "movie_number"
    
    let GREEN_THEME = 0
    let ORANGE_THEME = 1
    let RED_THEME = 2
    
    /** MOVIE NUMBER **/
    func setMovieNumber(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_MOVIE_NUMBER)
    }
    
    func getMovieNumber()->Int{
        return UserDefaults.standard.integer(forKey: KEY_MOVIE_NUMBER)
    }
    /** TAB OPEN CLOSE **/
    func setTabOpen(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_TAB_OPEN)
    }
    
    func getTabOpen()->Int{
        return UserDefaults.standard.integer(forKey: KEY_TAB_OPEN)
    }
    /** TAB ALPHA BETA **/
    func setTabVersion(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_TAB_VERSION)
    }
    
    func getTabVersion()->Int{
        return UserDefaults.standard.integer(forKey: KEY_TAB_VERSION)
    }
    /** ADVERTISING ON TOP NUMBER **/
    func setAdsNumberOnTop(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_ADS_NUMBER_ONTOP)
    }
    
    func getAdsNumberOnTop()->Int{
        return UserDefaults.standard.integer(forKey: KEY_ADS_NUMBER_ONTOP)
    }
    /** ADVERTISING NUMBER **/
    func setAdsNumber(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_ADS_NUMBER)
    }
    
    func getAdsNumber()->Int{
        return UserDefaults.standard.integer(forKey: KEY_ADS_NUMBER)
    }
    
}
