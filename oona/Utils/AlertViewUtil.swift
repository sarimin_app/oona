//
//  AlertViewUtil.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma on 8/15/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit

class AlertViewUtil{
    
    static let sharedIns = AlertViewUtil()
    
    func showAlertFeedback(_ target: AnyObject, messageText: String, actionButton1:String, actionButton2:String){
        //let when = DispatchTime.now() + 0.15 // change 2 to desired number of seconds
        //DispatchQueue.main.asyncAfter(deadline: when) {
            let alert = UIAlertController(title: "", message: messageText, preferredStyle: .alert)
            
        alert.addAction(UIAlertAction(title: actionButton1, style: UIAlertAction.Style.destructive, handler: nil))
        
            
            if(actionButton2.isEmpty == false){
                alert.addAction(UIAlertAction(title: actionButton2, style: UIAlertAction.Style.destructive, handler: nil))
            }
            
            target.present(alert, animated: true, completion: nil)
        //}
    }
    
    typealias handlerAlert = (_ action: UIAlertAction)  -> Void
    
    func showAlertFeedback(_ target: AnyObject, messageText: String, actionButton1:String, actionButton2:String, handler1: @escaping handlerAlert, handler2: @escaping handlerAlert){
        let alert = UIAlertController(title: "", message: messageText, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: actionButton1, style: UIAlertAction.Style.default, handler: handler1))
        
        
        if(actionButton2.isEmpty == false){
            alert.addAction(UIAlertAction(title: actionButton2, style: UIAlertAction.Style.default, handler: handler2))
        }
        
        target.present(alert, animated: true, completion: nil)
    }
    
}
