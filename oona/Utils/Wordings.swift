//
//  Wordings.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 8/15/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import Foundation

class Wordings{
    //BUTTON TEXT
    public static let BTN_CLOSE: String = "Close"
    public static let BTN_CANCEL: String = "Cancel"
    public static let BTN_DONE: String = "Finish"
    public static let BTN_CHOOSE: String = "Choose"
    public static let BTN_NEXT_KYB: String = "Next >"
    public static let BTN_OK: String = "Ok"
    
    //COLORS CODE
    public static let COLOR_RED_MAIN: String = "#e80c00"
    
    public static let msg_please_wait = "Please Wait..."
    public static let msg_loading = "Loading Data..."
    public static let msg_sending = "Sending..."
    public static let msg_updating = "Updating..."
    public static let msg_saving = "Saving..."
    
    public static let msg_connection_error : String = "Sorry, there was a problem accessing our system, please try again"
    public static let msg_server_error : String = "Sorry, there was a problem with our system, please try again"
    public static let msg_timeout_error : String = "Sorry, the time limit for the connection to the system has expired, please try again"
    public static let msg_internet_error : String = "Sorry, there is no mobile internet connection or Wi-fi on your mobile"
    public static let msg_maintenance : String = "Sorry, we are currently carrying out regular maintenance on our server. please try again later"
    
    public static let msg_no_response_api : String = "Sorry, you didn't get a response from the server, please try again"
    
     public static let msg_no_internet_connection : String = "No internet connection found, please check your mobile data or Wi-fi connection"
    
    public static let text_local_code_area = "+62"
    
    public static let msg_phone_starts_with : String = "Your mobile number must begin with number 8"
    
    public static let msg_input_phone : String = "Please enter your mobile number"
    public static let msg_phone_starts_with_local_area : String = "Fill in your Mobile Number with the prefix 0, +62, or 62"
    
    public static let msg_input_username : String = "Please enter your Username"
    public static let msg_input_password : String = "Please enter your password"
    public static let msg_wrong_password : String = "Incorrect password, please correct it"
    
    public static let msg_wrong_email_format : String = "Invalid email, please correct it"
    public static let msg_input_email : String = "Email must be filled in"
    public static let msg_input_notes : String = "Please fill in the message you want to convey"
    public static let msg_min_character_input_notes = "Message length at least 10 characters"
    public static let msg_input_phone_is_empty: String = "Please enter your mobile number correctly"
    
    public static let msg_input_pin_is_empty: String = "Please enter your mobile number correctly"
    public static let msg_input_pin_correctly: String = "Please enter your 4 digit PIN"
    public static let msg_input_confirm_pin_is_empty: String = "Please enter your PIN confirmation"
    
    public static let msg_input_confirm_pin_correctly: String = "Please enter your 4 digit PIN confirmation"
    
    public static let msg_pin_not_same: String = "Your confirmation PIN is not match"
    
    public static let msg_input_otp_isempty: String = "You have not entered the verification code"
    public static let msg_input_otp_length: String = "The verification code must be 4 digits"
    
    public static let msg_input_dob : String = "Please fill in your date of birth"
    
    public static let msg_error_tnc : String = "You must agree to the Dago Maps application Terms of Use and Privacy Policy"
    
    
    public static let text_how_to_apply_step_0 : String = "Terms:"
    public static let text_how_to_apply_step_1 : String = "Prepare the required documents:"
    public static let text_how_to_apply_step_2 : String = "Visit our closest partner shop"
    public static let text_how_to_apply_step_3 : String = "Choose the item you want:"
    public static let text_how_to_apply_step_4 : String = "Submit an installment application with our Sales Agent. \nThe approval process is only about 30 minutes"
    public static let text_how_to_apply_step_5 : String = "Take your stuff home!"
    
    
    public static let msg_register_completed : String = "Registration Successfull\nThank You"
    public static let msg_success_change_pin : String = "Successfully change the PIN"
    public static let msg_success_send_request_call_me : String = "Send successfully.\nYou will be contacted\nby our Customer Service"
    public static let msg_success_send_request_flexifast : String = "Send successfully.\nYou will be contacted\nby our Telesales"
    
    
    public static let msg_no_installment_schedule : String = "There is no payment schedule data"
    public static let msg_no_flexifast_offer : String = "There is no FlexiFast offer for you"
    public static let msg_no_active_contracts : String = "You do not have an active contract"
    public static let msg_no_finished_contracts : String = "You do not have a paid off contract"
    public static let msg_no_submission_contracts : String = "You have no contract submission data"
    
    
    public static let msg_re_login : String = "Please login again"
    public static let msg_timeout_otp : String = "Time is up, please resend SMS"
    public static let msg_failed_request_otp : String = "Failed to send verification code, please send sms again"
    public static let msg_otp_code_already_sent : String = "Verification code has been sent to " + " "
    public static let msg_loading_nearby_search : String = "Looking for the closest location ..."
    public static let msg_current_location_not_found : String = "Your location is not found, check your GPS and try again"
    
    public static let text_currency : String =  "Rp"
    
    public static let msg_error_fill_phone_number : String = "Please fill in your mobile number"
    
    public static let msg_error_phone_number_starts_with : String =  "Your mobile number must begin with number 8"
    
    
    public static let msg_error_phone_number_length : String =  "Enter your mobile number length correctly"
    
    public static let mesg_error_pin_length : String =  "The PIN code must be 4 digits"
    
    public static let msg_update_application : String =  "Please update your Dago Maps application"
}
