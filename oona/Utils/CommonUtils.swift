//
//  CommonUtils.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma on 8/15/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit
import CoreTelephony
import Contacts

class CommonUtils : UIViewController, UITextFieldDelegate{
    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }
    static let shared = CommonUtils()
    
    
    private static var textFields = [UITextField]()
    private static var textViews = [UITextView]()
    private static var textField = UITextField()
    private static var textView = UITextView()
    var days = [String]()
    var futuredays = [String]()
    var reversedDays = [String]()
    
    func getCurrencyLocaleFormat(amount: Double) -> String{
        let formatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "id_ID") as Locale
        formatter.numberStyle = .currency
        
        
        if let formattedTipAmount = formatter.string(from: amount as NSNumber) {
            let addSpaceSymbol = formattedTipAmount.replacingOccurrences(of: "Rp", with: "Rp ")
            return addSpaceSymbol
        }else{
            return ""
        }
    }
    
    func getCurrencyLocaleFormatWithoutSymbol(amount: Double) -> String{
        let formatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "id_ID") as Locale
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        
        if let formattedTipAmount = formatter.string(from: amount as NSNumber) {
            return formattedTipAmount
        }else{
            return ""
        }
    }

    public static func addNextButtonOnKeybaord(pos: Int, textFields: [UITextField]){
        self.textFields = textFields
        
        for index in 0..<textFields.count{
            
            let nextToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y:0, width:320, height:50))
            nextToolbar.barStyle = UIBarStyle.default
            
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            
            let next: UIBarButtonItem = UIBarButtonItem(title: Wordings.BTN_NEXT_KYB, style: UIBarButtonItem.Style.done, target: self, action: #selector(CommonUtils.nextButtonAction))
            
            next.tintColor = UIColor(string: Constant.sharedIns.color_black)
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(next)
            
            nextToolbar.items = items
            nextToolbar.sizeToFit()
            
            self.textFields[index].inputAccessoryView = nextToolbar
        }
    }
    
    @objc public static func nextButtonAction(){
        for index in 0..<textFields.count{
            
            if(textFields[index].isFirstResponder){
                if(index < textFields.count - 1){
                    self.textFields[index + 1].becomeFirstResponder()
                    break
                }
            }
        }
    }
    
    public static func addNextButtonWithTextViewOnKeybaord(pos: Int, textFields: [UITextField]){
        self.textFields = textFields
        
        for index in 0..<textFields.count{
            
            let nextToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y:0, width:320, height:50))
            nextToolbar.barStyle = UIBarStyle.default
            
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            
            let next: UIBarButtonItem = UIBarButtonItem(title: Wordings.BTN_NEXT_KYB, style: UIBarButtonItem.Style.done, target: self, action: #selector(CommonUtils.nextButtonActionWithTextView))
            
            next.tintColor = UIColor(string: Constant.sharedIns.color_black)
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(next)
            
            nextToolbar.items = items
            nextToolbar.sizeToFit()
            
            self.textFields[index].inputAccessoryView = nextToolbar
        }
    }
    
    @objc public static func nextButtonActionWithTextView(){
        for index in 0..<textFields.count{
            print("textFields.count:\(textFields.count)")
            if(textFields[index].isFirstResponder){
                if(index < textFields.count - 1){
                    self.textFields[index + 1].becomeFirstResponder()
                    break
                }else if(index < textFields.count){
//                    self.textFields[index].resignFirstResponder()
                    self.textView.becomeFirstResponder()
                    break
                }
            }
        }
    }
    
    
    public static func addDoneButtonOnKeyboard(textField: UITextField){
        self.textField = textField
            
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: Wordings.BTN_DONE, style: UIBarButtonItem.Style.done, target: self, action: #selector(CommonUtils.doneButtonAction))
        
        done.tintColor = UIColor(string: Constant.sharedIns.color_black)
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textField.inputAccessoryView = doneToolbar
        
    }
    
    public static func addDoneButtonWithTextViewOnKeyboard(textView: UITextView){
        self.textView = textView
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: Wordings.BTN_DONE, style: UIBarButtonItem.Style.done, target: self, action: #selector(CommonUtils.doneButtonActionTextView))
        
        done.tintColor = UIColor(string: Constant.sharedIns.color_black)
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textView.inputAccessoryView = doneToolbar
        
    }
    
    @objc public static func doneButtonAction(){
        //self.textField.resignFirstResponder()
        self.textField.endEditing(true)
    }
    
    @objc public static func doneButtonActionTextView(){
        //self.textView.resignFirstResponder()
        self.textView.endEditing(true)
    }
    
    public static func getDeviceInfo()->[String : Any]{
        
        let IMEI = Constant.getDeviceImei()
    
        let OS_Version = UIDevice.current.systemVersion
        let model = UIDevice.current.modelName
        
        let manufacturer = "APPLE"
        
        let serialNumber = Constant.GUIDString()
        
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        var providerName = carrier?.carrierName
        
        let RAM = ProcessInfo.processInfo.physicalMemory
        
        var totalDiskMemory = Constant.getTotalSize()
        var availableDiskMemory = Constant.deviceRemainingFreeSpaceInBytes()
        
        if providerName == nil{
            providerName = ""
        }
        
        if totalDiskMemory == nil{
            totalDiskMemory = 0
        }
        
        if availableDiskMemory == nil{
            availableDiskMemory = 0
        }
        
        let dictionary = [
            "imei": IMEI,
            "osVersion": OS_Version,
            "model" : model,
            "manufacturer": manufacturer,
            "serialNumber": serialNumber,
            "career": providerName!,
            "ram": RAM,
            "totalDiskMemory": totalDiskMemory!,
            "availableDiskMemory": availableDiskMemory!
        ] as [String : Any]
        
        return dictionary
    }

    
    public static let COMMODITY_HANDPHONE : String =  "Handphone"
    public static let COMMODITY_PHONE : String =  "Phone"
    public static let COMMODITY_TABLET : String =  "Tablet"
    public static let COMMODITY_CAMERA : String = "Kamera"
    public static let COMMODITY_TV : String = "TV";
    public static let COMMODITY_TELEVISION : String = "Televisi";
    public static let COMMODITY_COMPUTER : String = "Laptop";
    public static let COMMODITY_ELECTRONIC : String = "Consumer Durables";
    public static let COMMODITY_ELECTRONIC_2 : String = "Electronic";
    public static let COMMODITY_FURNITURE : String = "Furniture";
    public static let COMMODITY_GADGETS : String = "Gadget";
    public static let COMMODITY_WASH_MACHINE : String = "cuci";
    
    public static let COM_TYPE_HANDPHONE : String = "CT_TE_MP";
    public static let COM_TYPE_GADGET : String = "CT_CT_MTAB";
    public static let COM_TYPE_LAPTOP : String = "CT_CT_LAP";
    public static let COM_TYPE_FURNITURE : String = "CT_FUR";
    public static let COM_TYPE_ELECTRONIC : String = "CT_HA_AC";
    public static let COM_TYPE_TV : String = "CT_HA_CTV";
    
    
    public static let SERVICE_GIFT_PAYMENT_1 : String = "GIFT-1";
    public static let SERVICE_GIFT_PAYMENT_2 : String = "GIFT-2";
    public static let SERVICE_ADLD : String = "ADLD";
    public static let SERVICE_AMAN : String = "AMAN";
    public static let SERVICE_ACTIVE : String = "ACTIVE";
    public static let SERVICE_ELIGIBLE : String = "ELIGIBLE";
    
    public static let PRODUCT_MPF : String = "MPF";
    
    //Commodity Category to determine icon of commodity
    static func getCommodityCategory(pos: Int) -> String{
        let commodityCat = ""
        return commodityCat
    }
    
    //Commodity Type to determine icon of commodity
    static func getCommodityType(pos: Int)->String{
        let commodityType = ""
        return commodityType
    }
    
    public static func getBigCommodityIcon(commodityCategory: String?, commodityType: String?, productCat: String?)->String{
        var iconName = ""
    
        if (commodityCategory != nil) {
            if(productCat==nil){
                //DEFAULT ICON
                return "ic_all_commodity"
            }else if(productCat?.contains(PRODUCT_MPF))! {
                return "ic_coin_flexifast_white"
            }else if (commodityCategory?.contains(COMMODITY_COMPUTER))! ||
                (commodityType?.contains(COM_TYPE_LAPTOP))! {
                return "ic_com_laptop"
            }else if (commodityCategory?.contains(COMMODITY_ELECTRONIC))! ||
                (commodityType?.contains(COM_TYPE_ELECTRONIC))!{
                if (commodityType?.contains(COMMODITY_CAMERA))! {
                    return "ic_com_camera"
                }else if (commodityType?.contains(COMMODITY_WASH_MACHINE))! {
                    return "ic_com_wash_machine"
                }
                iconName = "ic_com_refrigerator"

            } else if (commodityCategory?.contains(COMMODITY_FURNITURE))! ||
                (commodityType?.contains(COM_TYPE_FURNITURE))! {
                iconName = "ic_com_furniture"
            } else if (commodityCategory?.contains(COMMODITY_PHONE))! ||  (commodityCategory?.contains(COMMODITY_HANDPHONE))!{
                iconName = "ic_com_smartphone"
            } else if (commodityCategory?.contains(COMMODITY_TV))! ||
                (commodityType?.contains(COM_TYPE_TV))! {
                iconName = "ic_com_led_tv"
            } else if (commodityCategory?.contains(COMMODITY_GADGETS))! ||
                (commodityType?.contains(COM_TYPE_GADGET))! {
                iconName = "ic_com_tablet"
            } else {
                iconName = "ic_all_commodity"
            }

        } else {
            //DEFAULT ICON
            iconName = "ic_all_commodity"
        }

        return iconName;
    }
    
    public static func getCommodityListName()->[String]{
        return ["Handphone", "Gadget", "Komputer atau Laptop", "Furnitur", "Peralatan Rumah Tangga", "Televisi"]
    }
    
    func removeDoubleDecimal(num : Double)->String{
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        
        return formatter.string(for: num)!
    }
    
    struct AppUtility {
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        
        /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            
            self.lockOrientation(orientation)
            
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
        
    }
    
    func createContact(surname: String, lastName: String, birthday: String, homePhone: String, mobilePhone: String, homeEmail: String, workEmail: String)->CNMutableContact{
        
        let contact = CNMutableContact()
        contact.namePrefix = surname
        contact.nameSuffix = lastName
        contact.organizationName = "Apple"
        contact.jobTitle = "Software Engineer"
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let birthday = dateFormatter.date(from: birthday)
        
        let birthdayComponents = Calendar.current.dateComponents([.year, .month, .day], from: birthday!)
        contact.birthday = birthdayComponents
        
        let phone = CNLabeledValue(
            label:CNLabelPhoneNumberMain,
            value:CNPhoneNumber(stringValue:homePhone))
        
        let mobileNumber = CNLabeledValue(
            label:CNLabelPhoneNumberMobile,
            value:CNPhoneNumber(stringValue:mobilePhone))
        
        contact.phoneNumbers = [phone, mobileNumber]
        
        let WorkEmail = CNLabeledValue(label:CNLabelWork, value: workEmail as NSString)
        contact.emailAddresses = [WorkEmail]
        
        let address = CNMutablePostalAddress()
        address.street = "Infinity Loop"
        address.city = "Cupertino"
        address.state = "CA"
        address.postalCode = "95014"
        address.country = "United States"
        
        let labeledAddress = CNLabeledValue<CNPostalAddress>(label: CNLabelHome, value: address)
        
        contact.postalAddresses = [labeledAddress]
        
        return contact
    }
    
    func getLast14Dates()->[String]{

        let cal = Calendar.current
        var date = cal.startOfDay(for: Date())
        days.removeAll()
        reversedDays.removeAll()
        futuredays.removeAll()
        for _ in 1 ... 14 {
            let day = cal.component(.day, from: date - 1)
            days.append("\(day)")
            date = cal.date(byAdding: .day, value: -1, to: date)!
        }
        for arrayIndex in stride(from: days.count - 1, through: 0, by: -1) {
            reversedDays.append(days[arrayIndex])
        }
        return reversedDays
    }
    
    func getFuture14Dates()->[String]{

        let cal = Calendar.current
        var date = cal.startOfDay(for: Date())
        days.removeAll()
        reversedDays.removeAll()
        futuredays.removeAll()
        for _ in 1 ... 15 {
            let day = cal.component(.day, from: date + 1)
            futuredays.append("\(day)")
            date = cal.date(byAdding: .day, value: 1, to: date)!
        }
        return futuredays
    }
    
    func checkAppVersion() {
        _ = try? isUpdateAvailable { (update, error) in
            if let error = error {
                print("error app version:\(error)")
            } else if let update = update {
                print("update app version:\(update)")
//                self.showMessage(title: "New Version Available", msg: Wordings.msg_update_application)
            }
        }
    }
    
    func isUpdateAvailable(completion: @escaping (Bool?, Error?) -> Void) throws -> URLSessionDataTask {
        guard let info = Bundle.main.infoDictionary,
            let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject,
            let currentVersion = nsObject as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                throw VersionError.invalidBundleInfo
        }
        print("app version is:\(currentVersion)")
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                if let error = error { throw error }
                guard let data = data else { throw VersionError.invalidResponse }
                let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                guard let result = (json?["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String else {
                    throw VersionError.invalidResponse
                }
                completion(version != currentVersion, nil)
            } catch {
                completion(nil, error)
            }
        }
        task.resume()
        return task
    }
    
    func showMessage(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Wordings.BTN_CLOSE, style: UIAlertAction.Style.default, handler: nil))
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
    
    public static func verifyUrl(url: String?) -> Bool {
        let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: url)
        return result
    }
    
    func getCountryCallingCode(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
    }
    
    func fahrenheitToCelcius (Degree: Double) -> Double {
        let celcius = (Degree - 32) / 1.8
        return celcius
    }
    
    func celciusToFahrenheit (Degree: Double) -> Double {
        let fahrenheit = (Degree * 1.8) + 32
        return fahrenheit
    }

}
