//
//  AppController.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 8/15/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit
//import Firebase
import UserNotifications

class AppController{
    
    static let sharedInstance = AppController()

}

class CustomUIButton: UIButton{
    var customInputView: UIView?
    var customInputAccessoryView: UIView?
    
    override var inputView: UIView? {
        get {
            return self.customInputView
        }

        set {
            self.customInputView = newValue
        }
    }

    override var inputAccessoryView: UIView? {
        get {
            return self.customInputAccessoryView
        }

        set {
            self.customInputAccessoryView = newValue
        }
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
}
