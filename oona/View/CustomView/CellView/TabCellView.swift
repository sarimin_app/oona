//
//  TabCellView.swift
//  oona
//
//  Created by Oscar Perdanakusuma Adipati on 17/03/19.
//  Copyright © 2019 Sarimin App. All rights reserved.
//

import Foundation
import UIKit

class TabCellView: UICollectionViewCell {
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var CellView: UIView!
}
