//
//  AdvertisingViewController.swift
//  oona
//
//  Created by Oscar Perdanakusuma Adipati on 16/03/19.
//  Copyright © 2019 Sarimin App. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit
import AVFoundation

class AdvertisingViewController: UIViewController {
    @IBOutlet weak var mainView: UIView!
    var homeController : ViewController?
    var avPlayer: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtils.AppUtility.lockOrientation(.landscape)
        advertising()
        self.avPlayer?.play()
        Timer.scheduledTimer(timeInterval: 15,
                             target: self,
                             selector: #selector(self.showMainVideo),
                             userInfo: nil,
                             repeats: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtils.AppUtility.lockOrientation(.landscape)
        self.avPlayer?.pause()
    }
    
    func advertising() {
        self.mainView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let filepath: String? = Bundle.main.path(forResource: "ads\(CustomUserDefaults.shared.getAdsNumber())", ofType: "mp4")
        let fileURL = URL.init(fileURLWithPath: filepath!)
        avPlayer = AVPlayer(url: fileURL)
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayer?.currentItem)
        let avPlayerController = AVPlayerViewController()
        avPlayerController.player = avPlayer
        avPlayerController.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        avPlayerController.view.contentMode = .scaleAspectFill
        avPlayerController.view.backgroundColor = .black
        //  hide show control
        avPlayerController.showsPlaybackControls = false
        // play video
        avPlayerController.player?.play()
        self.addChild(avPlayerController)
        self.view.addSubview(avPlayerController.view)
        avPlayerController.didMove(toParent: self)
//        let movieUrl:NSURL? = NSURL( string : Constant.getAPIAdvertising())
//        if let url = movieUrl{
//            self.avPlayer = AVPlayer(url : url as URL )
//            let avPlayerController = AVPlayerViewController()
//            avPlayerController.player = avPlayer
//            avPlayerController.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
//            avPlayerController.view.contentMode = .scaleAspectFill
//            avPlayerController.view.backgroundColor = .black
//            //  hide show control
//            avPlayerController.showsPlaybackControls = false
//            // play video
//            avPlayerController.player?.play()
//            self.addChild(avPlayerController)
//            self.view.addSubview(avPlayerController.view)
//            avPlayerController.didMove(toParent: self)
//        }
    }
    
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        self.avPlayer?.seek(to: CMTime.zero)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func showMainVideo() {
        print("already 15 seconds")
        self.avPlayer?.seek(to: CMTime.zero)
        dismiss(animated: true, completion: nil)
    }
    
}


