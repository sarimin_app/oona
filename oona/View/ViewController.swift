//
//  ViewController.swift
//  oona
//
//  Created by Oscar Perdanakusuma Adipati on 15/03/19.
//  Copyright © 2019 Sarimin App. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit
import AVFoundation

class ViewController: UIViewController {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var barView: UIView!
    @IBOutlet weak var barCellView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var imgAdvertising: UIImageView!
    @IBOutlet weak var imgTabVersion: UIImageView!
    var avPlayer: AVPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()
        CustomUserDefaults.shared.setTabOpen(0)
        barView.isHidden = true
        imgAdvertising.isHidden = true
        imgAdvertising.contentMode = UIView.ContentMode.scaleAspectFit
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "TabCell", bundle: nil), forCellWithReuseIdentifier: "collectionCell")
        movieOne()
        setRoundedViewWithBorder(view: barView, borderColor: Constant.sharedIns.color_white, bgColor: Constant.sharedIns.color_grey_500)
        setRoundedView(view: collectionView, bgColor: Constant.sharedIns.color_grey_500)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        imgAdvertising.setCircleBackgroundForView(view: imgAdvertising, bgColor: UIColor.clear)
        imgAdvertising.setCircleStrokeForView(view: imgAdvertising, strokeWidth: 0.5, strokeColor: UIColor.lightGray)
        addTapEventForView(view: imgMenu, action: #selector(tabOpen))
        addTapEventForView(view: imgTabVersion, action: #selector(tabVersion))
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 10.00) {
            self.advertisingShowOnTop()
        }
    }

    override var shouldAutorotate: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtils.AppUtility.lockOrientation(.landscape)
        self.avPlayer?.play()
        Timer.scheduledTimer(timeInterval: 30,
                             target: self,
                             selector: #selector(self.showAdvertising),
                             userInfo: nil,
                             repeats: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtils.AppUtility.lockOrientation(.landscape)
        self.avPlayer?.pause()
        if (CustomUserDefaults.shared.getAdsNumber() == 0) {
            CustomUserDefaults.shared.setAdsNumber(1)
        } else if (CustomUserDefaults.shared.getAdsNumber() == 1) {
            CustomUserDefaults.shared.setAdsNumber(2)
        } else if (CustomUserDefaults.shared.getAdsNumber() == 2) {
            CustomUserDefaults.shared.setAdsNumber(3)
        } else if (CustomUserDefaults.shared.getAdsNumber() == 3) {
            CustomUserDefaults.shared.setAdsNumber(1)
        }
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.right {
            print("Swipe Right")
            movieOne()
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.left {
            print("Swipe Left")
            movieTwo()
        }
    }
    
    @objc func tabOpen() {
        if (CustomUserDefaults.shared.getTabOpen() == 0) {
            CustomUserDefaults.shared.setTabOpen(1)
            barView.isHidden = false
        } else if (CustomUserDefaults.shared.getTabOpen() == 1) {
            CustomUserDefaults.shared.setTabOpen(0)
            barView.isHidden = true
        }
    }
    
    @objc func tabVersion() {
        if (CustomUserDefaults.shared.getTabVersion() == 0) {
            CustomUserDefaults.shared.setTabVersion(1)
            setRoundedViewWithBorder(view: barView, borderColor: Constant.sharedIns.color_white, bgColor: Constant.sharedIns.color_grey_500)
            setRoundedView(view: collectionView, bgColor: Constant.sharedIns.color_grey_500)
        } else if (CustomUserDefaults.shared.getTabVersion() == 1) {
            CustomUserDefaults.shared.setTabVersion(0)
            setRoundedViewWithBorder(view: barView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_green_500)
            setRoundedView(view: collectionView, bgColor: Constant.sharedIns.color_green_500)
        }
    }
    
    @objc func advertisingShowOnTop() {
        imgAdvertising.isHidden = false
        if (CustomUserDefaults.shared.getAdsNumberOnTop() == 0) {
            CustomUserDefaults.shared.setAdsNumberOnTop(1)
        } else if (CustomUserDefaults.shared.getAdsNumberOnTop() == 1) {
            CustomUserDefaults.shared.setAdsNumberOnTop(2)
        } else if (CustomUserDefaults.shared.getAdsNumberOnTop() == 2) {
            CustomUserDefaults.shared.setAdsNumberOnTop(3)
        } else if (CustomUserDefaults.shared.getAdsNumberOnTop() == 3) {
            CustomUserDefaults.shared.setAdsNumberOnTop(1)
        }
        let adsImage: UIImage = UIImage(named: "advertising\(CustomUserDefaults.shared.getAdsNumberOnTop())")!
        imgAdvertising.image = adsImage
        Timer.scheduledTimer(timeInterval: 10,
                             target: self,
                             selector: #selector(self.advertisingHiddenOnTop),
                             userInfo: nil,
                             repeats: false)
    }
    
    @objc func advertisingHiddenOnTop() {
        imgAdvertising.isHidden = true
        Timer.scheduledTimer(timeInterval: 10,
                             target: self,
                             selector: #selector(self.advertisingShowOnTop),
                             userInfo: nil,
                             repeats: false)
    }
    
    @objc func showAdvertising() {
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.ADV_NAV)
        present(vc, animated: false, completion: nil)
    }
    
    func movieOne() {
        CustomUserDefaults.shared.setMovieNumber(0)
        self.mainView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.mainView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let filepath: String? = Bundle.main.path(forResource: "video1", ofType: "mp4")
        let fileURL = URL.init(fileURLWithPath: filepath!)
        avPlayer = AVPlayer(url: fileURL)
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayer?.currentItem)
        let avPlayerController = AVPlayerViewController()
        avPlayerController.player = avPlayer
        avPlayerController.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        avPlayerController.view.contentMode = .scaleAspectFill
        avPlayerController.view.backgroundColor = .black
        //  hide show control
        avPlayerController.showsPlaybackControls = false
        // play video
        avPlayerController.player?.play()
        self.addChild(avPlayerController)
        self.mainView.addSubview(avPlayerController.view)
        avPlayerController.didMove(toParent: self)
//        let movieUrl:NSURL? = NSURL( string : Constant.getAPIMovieOne())
//        if let url = movieUrl{
//            self.avPlayer = AVPlayer(url : url as URL )
//            let avPlayerController = AVPlayerViewController()
//            avPlayerController.player = avPlayer
//            avPlayerController.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
//            avPlayerController.view.contentMode = .scaleAspectFill
//            avPlayerController.view.backgroundColor = .black
//            //  hide show control
//            avPlayerController.showsPlaybackControls = false
//            // play video
//            avPlayerController.player?.play()
//            self.addChild(avPlayerController)
//            self.mainView.addSubview(avPlayerController.view)
//            avPlayerController.didMove(toParent: self)
//        }
    }
    
    func movieTwo() {
        CustomUserDefaults.shared.setMovieNumber(1)
        self.mainView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.mainView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let filepath: String? = Bundle.main.path(forResource: "video2", ofType: "mp4")
        let fileURL = URL.init(fileURLWithPath: filepath!)
        avPlayer = AVPlayer(url: fileURL)
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayer?.currentItem)
        let avPlayerController = AVPlayerViewController()
        avPlayerController.player = avPlayer
        avPlayerController.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        avPlayerController.view.contentMode = .scaleAspectFill
        avPlayerController.view.backgroundColor = .black
        //  hide show control
        avPlayerController.showsPlaybackControls = false
        // play video
        avPlayerController.player?.play()
        self.addChild(avPlayerController)
        self.mainView.addSubview(avPlayerController.view)
        avPlayerController.didMove(toParent: self)
//        let movieUrl:NSURL? = NSURL( string : Constant.getAPIMovieTwo())
//        if let url = movieUrl{
//            self.avPlayer = AVPlayer(url : url as URL )
//            let avPlayerController = AVPlayerViewController()
//            avPlayerController.player = avPlayer
//            avPlayerController.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
//            avPlayerController.view.contentMode = .scaleAspectFill
//            avPlayerController.view.backgroundColor = .black
//            //  hide show control
//            avPlayerController.showsPlaybackControls = false
//            // play video
//            avPlayerController.player?.play()
//            self.addChild(avPlayerController)
//            self.mainView.addSubview(avPlayerController.view)
//            avPlayerController.didMove(toParent: self)
//        }
    }
    
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        self.avPlayer?.seek(to: CMTime.zero)
        if (CustomUserDefaults.shared.getMovieNumber() == 0) {
            movieTwo()
        } else if (CustomUserDefaults.shared.getMovieNumber() == 1) {
            movieOne()
        }
        self.avPlayer?.play()
    }

}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCell: CGFloat = CGFloat(3)
        print("numberOfCell:\(numberOfCell)")
        print("WeeklyCollectionView.frame.size.width:\(barCellView.frame.size.width)")
        let cellWidth = barCellView.frame.size.width / numberOfCell - 18
        print("cellWidth:\(cellWidth)")
        return CGSize(width: cellWidth, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : TabCellView = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! TabCellView
        let index = indexPath.row
        cell.lblIndex.text = "\(index)"
        if (index == 0) {
            setRoundedViewWithBorder(view: cell.CellView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_blue)
        } else if (index == 1) {
            setRoundedViewWithBorder(view: cell.CellView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_red_main)
        } else if (index == 2) {
            setRoundedViewWithBorder(view: cell.CellView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_orange_main)
        }
        return cell
    }
    
    /// Select collection view cell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        if (index == 0) {
            
        } else if (index == 1) {
            
        } else if (index == 2) {
            
        }
    }
}
